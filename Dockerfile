FROM debian:jessie
MAINTAINER Leonardo Petrora

VOLUME ["/var/www"]

RUN apt-get update && \
    apt-get install -y \
      locales \
      apache2 \
      php5 \
      php5-cli \
      libapache2-mod-php5 \
      php5-gd \
      php5-json \
      php5-ldap \
      php5-mysql \
      php5-pgsql \
      php5-curl \
      php5-xdebug

COPY apache_default /etc/apache2/sites-available/000-default.conf
COPY run /usr/local/bin/run
COPY xdebug.ini /etc/php5/mods-available/xdebug.ini

RUN chmod +x /usr/local/bin/run
RUN a2enmod rewrite

EXPOSE 80
EXPOSE 9000
CMD ["/usr/local/bin/run"]
