apache2-php56-xdebug
===================================

Imagen de Docker basada en Debian Jessie, con PHP 5.6 corriento como módulo de Apache y xdebug.

Tags
-----

* latest: Debian 8 (Jessie), Apache 2.4, PHP 5.6.x (`error_reporting` configurable), xdebug

Uso
---
* Construccion
```bash
$ docker build -t YS/apache2-php56-xdebug:latest .
```


* Ejecucion (con todas las opciones)

```bash
$ docker run -d -p 8080:80 \
    -p 9000:9000
    -v /home/user/webroot:/var/www \
    -e PHP_ERROR_REPORTING='E_ALL & ~E_STRICT' \
    --add-host dockerhost:172.17.0.1 \
    --name mi-contenedor \
    ys/apache2-php56-xdebug
```

* `-v [local path]:/var/www` mapea el contenido del web root a la unidad local
* `-p [local port]:80` mapea un puerto local al puerto 80 del contenedor (Web server)
* `-p [local port]:9000` mapea un puerto local al puerto 9000 del contenedor (xdebug)
* `-e PHP_ERROR_REPORTING=[php error_reporting settings]` cambia el valor de  `error_reporting` en php.ini.
* `--add-host dockerhost:172.17.0.1 \` agregar al archivo host la IP de nuestra máquina (interfaz docker) para el host dockerhost
* `--name mi-contenedor` opcional, ponerle un nombre al container para que sea más facil iniciarlo con docker start / stop

### Acceso a los logs de apache

Apache está configurado para enviar el log de access y error a STDOUT. Para obtener la salida de log usar:

`docker logs -f container-id`


Paquetes instalados
-------------------
* Debian 8 (jessie)
* locales
* apache2
* php5
* php5-cli
* libapache2-mod-php5
* php5-gd
* php5-json
* php5-ldap
* php5-mysql
* php5-pgsql
* php5-curl
* php5-xdebug

Configuraciones
----------------

* Apache: .htaccess activado en webroot (mod_rewrite con AllowOverride all)
* php.ini:
  * display_errors = On
  * error_reporting = E_ALL (default, overridable per env variable)

Ejemplo de construcción y ejecución
-----------------------------------
* Construcción
```docker build -t ys/apache2-php56-xdebug:latest .```

* Ejecucion
```docker run -p 8080:80 -p 9001:9000 -v /home/leonardo/workspace/php/proyecto/:/var/www --add-host=dockerhost:172.17.0.1 --name mi-proyecto ys/apache2-php56-xdebug```
